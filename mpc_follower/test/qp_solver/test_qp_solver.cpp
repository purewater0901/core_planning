#include <ros/ros.h>
#include <cmath>
#include <gtest/gtest.h>
#include <iostream>

#include "mpc_follower/qp_solver/qp_solver_interface.h"
#include "mpc_follower/qp_solver/qp_solver_qpoases.h"

class TestSuite: public ::testing::Test 
{
public:
	TestSuite(){}
	~TestSuite(){}
};

TEST(TestSuite, TestInEqualityConstrainedQP)
{
    std::shared_ptr<QPSolverInterface> qpsolver_ptr;

    int max_iter = 500;
    qpsolver_ptr = std::make_shared<QPSolverQpoasesHotstart>(max_iter);

    Eigen::MatrixXd H = Eigen::MatrixXd::Zero(2, 2);
    H(0,0) = 1.0;
    H(0,1) = -1.0;
    H(1,0) = -1.0;
    H(1,1) = 2.0;

    Eigen::MatrixXd f = Eigen::MatrixXd::Zero(1, 2);
    f(0,0) = -2.0;
    f(0,1) = -6.0;

    Eigen::VectorXd lb = Eigen::VectorXd::Zero(2);
    lb(0) = -100.0;
    lb(1) = -100.0;

    Eigen::VectorXd ub = Eigen::VectorXd::Zero(2);
    ub(0) = 100.0;
    ub(1) = 100.0;

    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3, 2);
    A(0,0) = 1.0;
    A(0,1) = 1.0;
    A(1,0) = -1.0;
    A(1,1) = 2.0;
    A(2,0) = 2.0;
    A(2,1) = 1.0;

    Eigen::MatrixXd lbA = Eigen::MatrixXd::Zero(3, 1);
    lbA(0, 0) = -2.0;
    lbA(1, 0) = -2.0;
    lbA(2, 0) = -3.0;

    Eigen::MatrixXd ubA = Eigen::MatrixXd::Zero(3, 1);
    ubA(0, 0) = 2.0;
    ubA(1, 0) = 2.0;
    ubA(2, 0) = 3.0;

    Eigen::VectorXd Uex;
    if(qpsolver_ptr->solve(H, f.transpose(), A, lb, ub, lbA, ubA, Uex))
    {
        const double u0 = Uex(0);
        const double u1 = Uex(1);
        const double u0_matlab = 0.6667;
        const double u1_matlab = 1.3333;

        ASSERT_NEAR(u0, u0_matlab, std::fabs(u0_matlab * 0.1)/* error limit*/) << "u0 value differs more than 10% with matlab, check equation";
        ASSERT_NEAR(u1, u1_matlab, std::fabs(u1_matlab * 0.1)/* error limit*/) << "u1 value differs more than 10% with matlab, check equation";
    }
    else
        std::cout << "Error" << std::endl;
}

TEST(TestSuite, TestEqualityConstrainedQP)
{
    std::shared_ptr<QPSolverInterface> qpsolver_ptr;

    int max_iter = 500;
    qpsolver_ptr = std::make_shared<QPSolverQpoasesHotstart>(max_iter);

    Eigen::MatrixXd H = Eigen::MatrixXd::Zero(3, 3);
    H(0,0) = 1.0;
    H(0,1) = -1.0;
    H(0,2) = 1.0;
    H(1,0) = -1.0;
    H(1,1) = 2.0;
    H(1,1) = -2.0;
    H(2,0) = 1.0;
    H(2,1) = -2.0;
    H(2,1) = 4.0;

    Eigen::MatrixXd f = Eigen::MatrixXd::Zero(1, 3);
    f(0,0) = 2.0;
    f(0,1) = -3.0;
    f(0,2) = 1.0;

    Eigen::VectorXd lb = Eigen::VectorXd::Zero(3);
    lb(0) = 0.0;
    lb(1) = 0.0;
    lb(2) = 0.0;

    Eigen::VectorXd ub = Eigen::VectorXd::Zero(3);
    ub(0) = 1.0;
    ub(1) = 1.0;
    ub(2) = 1.0;

    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(1, 3);
    A(0,0) = 1.0;
    A(0,1) = 1.0;
    A(0,2) = 1.0;

    Eigen::MatrixXd lbA = Eigen::MatrixXd::Zero(1, 1);
    lbA(0, 0) = 1.0/2.0;

    Eigen::MatrixXd ubA = Eigen::MatrixXd::Zero(1, 1);
    ubA(0, 0) = 1.0/2.0;

    Eigen::VectorXd Uex;
    if(qpsolver_ptr->solve(H, f.transpose(), A, lb, ub, lbA, ubA, Uex))
    {
        const double u0 = Uex(0);
        const double u1 = Uex(1);
        const double u2 = Uex(2);
        const double u0_matlab = 0.0;
        const double u1_matlab = 0.5;
        const double u2_matlab = 0.0;

        ASSERT_NEAR(u0, u0_matlab, std::fabs(u0_matlab * 0.1)/* error limit*/) << "u0 value differs more than 10% with matlab, check equation";
        ASSERT_NEAR(u1, u1_matlab, std::fabs(u1_matlab * 0.1)/* error limit*/) << "u1 value differs more than 10% with matlab, check equation";
        ASSERT_NEAR(u2, u2_matlab, std::fabs(u2_matlab * 0.1)/* error limit*/) << "u1 value differs more than 10% with matlab, check equation";
    }
    else
        std::cout << "Error" << std::endl;
}


int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    ros::init(argc, argv, "TestNode");
    return RUN_ALL_TESTS();
}